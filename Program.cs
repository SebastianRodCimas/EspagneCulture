﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EspagneCulture;
using EspagneCulture.Model;

namespace ConsoleEspagneCulture
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Base de données EspanaCultura");

            ModelContext context = new ModelContext();

            Console.WriteLine("Affichage des étudiants étant en A2");
            IQueryable<Etudiant> listeetudiant = context.Etudiants
                .Where(e => e.Id_classe == 2 && e.ID_classe_Se_trouve == 2);
                
            ShowEtudiants(listeetudiant.ToList());

            Console.WriteLine("Affichage les identifiants des classes en dessous de 4 et au dessus de  1");
            IQueryable<Classe> listeclasse = context.Classes
                .Where(c => c.Nom.Count() < 4 && c.Nom.Count() > 1);
              
            ShowClasse(listeclasse.ToList());

            Console.WriteLine("Affichage de la matière de 'Noticia' ");
            IQueryable<Matieres> listematiere = context.Matieres
                .Where(m => m.Nom.Contains("Noticia"));

            ShowMatiere(listematiere.ToList());

            Console.WriteLine("Professeurs ayant un nom de famille commençant par " +
                "la lettre 'M'" +" et un prénom par la lettre 'T' ");
            IQueryable<Professeur> listeprofesseur = context.Professeurs
                 .Where(p => p.Nom.StartsWith("M"))
                 .Where(p => p.Prenom.StartsWith("T"));
            ShowProfesseur(listeprofesseur.ToList());

            GestionCulture gestionCulture = new GestionCulture();

            List<Professeur> liste = new List<Professeur>();
            liste.Add(context.Professeurs.FirstOrDefault(p => p.Id.Equals(7)));
            liste.Add(context.Professeurs.FirstOrDefault(p => p.Nom.Equals("Cimas")));
            liste.Add(context.Professeurs.FirstOrDefault(p => p.Prenom.Equals("Oscar")));

            gestionCulture.AddProfesseur(liste);

        }

        static void ShowEtudiants(List<Etudiant> listeetudiant)
        {
            foreach (var etudiants in listeetudiant)
            {
                Console.WriteLine($"Nom , {etudiants.Nom} : Prenom = {etudiants.Prenom}");
            }
        }
        static void ShowClasse(List<Classe> listeclasse)
        {
            foreach (var classe in listeclasse)
            {
                Console.WriteLine($"Nom {classe.Nom} : Id = {classe.Id}");
            }
        }
        static void ShowMatiere(List<Matieres> listematiere)
        {
            foreach (var matiere in listematiere)
            {
                Console.WriteLine($"Nom {matiere.Nom}: Id = {matiere.Id}");
            }
        }

        static void ShowProfesseur(List<Professeur> listeprofesseur)
        {
            foreach (var professeur in listeprofesseur)
            {
                Console.WriteLine($"Nom {professeur.Nom}: Prénom = {professeur.Prenom}");
            }
        }
    }
}

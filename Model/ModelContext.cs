using EspagneCulture.Model;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace EspagneCulture.Model
{
    public partial class ModelContext : DbContext
    {
        public ModelContext()
            : base("name=ModelCulture")
        {
        }

        public virtual DbSet<Classe> Classes { get; set; }
        public virtual DbSet<Etudiant> Etudiants { get; set; }
        public virtual DbSet<Matieres> Matieres { get; set; }
        public virtual DbSet<Professeur> Professeurs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Classe>()
                .Property(e => e.Nom)
                .IsUnicode(false);

            modelBuilder.Entity<Classe>()
                .HasMany(e => e.Etudiants)
                .WithRequired(e => e.Classe)
                .HasForeignKey(e => e.ID_classe_Se_trouve)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Etudiant>()
                .Property(e => e.Nom)
                .IsUnicode(false);

            modelBuilder.Entity<Etudiant>()
                .Property(e => e.Prenom)
                .IsUnicode(false);

            modelBuilder.Entity<Matieres>()
                .Property(e => e.Nom)
                .IsUnicode(false);

            modelBuilder.Entity<Professeur>()
                .Property(e => e.Nom)
                .IsUnicode(false);

            modelBuilder.Entity<Professeur>()
                .Property(e => e.Prenom)
                .IsUnicode(false);
        }
    }
}

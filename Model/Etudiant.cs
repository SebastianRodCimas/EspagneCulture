namespace EspagneCulture.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("espanacultura.Etudiant")]
    public partial class Etudiant
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id_classe { get; set; }

        [Required]
        [StringLength(25)]
        public string Nom { get; set; }

        [Required]
        [StringLength(25)]
        public string Prenom { get; set; }

        public int ID_classe_Se_trouve { get; set; }

        public virtual Classe Classe { get; set; }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EspagneCulture.Model;
namespace EspagneCulture
{
    public class GestionCulture
    {
        ModelContext context = new ModelContext();

        public List<Etudiant> Products()
        {
            // affiche la liste des étudiants
            return context.Etudiants.ToList();
        }

        public Etudiant AddEtudiants(Etudiant etudiants)
        {
            // ajoute un etudiant à l'ORM
            context.Etudiants.Add(etudiants);
            // confirme les changements
            if (context.SaveChanges() > 0)
                return etudiants;
            return null;

        }
        public bool EditEtudiants(Etudiant etudiants)
        {
             // modifié un étudiant
            context.Entry(etudiants).State = EntityState.Modified;
            // confirme les changements
            return (context.SaveChanges() > 0);
        }

        public bool DeleteEtudiants(Etudiant etudiants)
        {
            if (etudiants != null)
            {
                // Supprime l'étudiant de l'ORM
                context.Etudiants.Remove(etudiants);
                // Valide les changement dans la base de données
                return (context.SaveChanges() > 0);
            }
            return false;
        }

        public Etudiant SearchEtudiant(int Id)
        {

            //recherche l'étudiant avec son id
            return context.Etudiants.Find(Id);
        }
        public List<Etudiant> SearchEtudiantWithName(string nom)
        {

            //recherche l'étudiant avec son nom
            var liste = context.Etudiants.Where(e => e.Nom.Contains(nom));
            return liste.ToList();
        }

        public void AddProfesseur(List<Professeur> listeprofesseur)
        {
        
            // 1 - On ajoute un Professeur avec ces éléments
            Professeur professeur = new Professeur()
            {
               Id = 7,
               Nom = "Cimas",
               Prenom = "Oscar",
            };
            context.Professeurs.Add(professeur);
            // 2 - On sauvegarde pour obtenir son id
            context.SaveChanges();
            // 3 - On crée un dictionnaire avec la table Professeur
            Dictionary<int, Professeur> dico = new Dictionary<int, Professeur>();
            // 4 - On parcours la liste
            foreach (Professeur professeurs in listeprofesseur)
            {
                // 5 - Si le professeur est présent dans le dictionnaire alors il est affiché dans la console
                if (dico.ContainsKey(professeurs.Id))
                {
                    dico[professeurs.Id].Id++;
                }
                else
                {
                    // sinon on l'ajoute
                    Professeur professeur1 = new Professeur()
                    {
                       
                        Id = professeurs.Id,
                        Nom = professeurs.Nom,
                        Prenom= professeurs.Prenom,
                    };
                    dico.Add(professeurs.Id,professeur1);
                }
            }

            // 6 - On parcours le dictionnaire pour liste de la table Professeur
            foreach (var keyValue in dico)
            {
                context.Professeurs.Add(keyValue.Value);
            }
            // On sauvegarde dans la BDD
            context.SaveChanges();
        }
    }
}